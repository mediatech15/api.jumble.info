# api.jumble.info

# Description

;lkasdjfflk;asdjflkjsafd;

# API Layout

```
/ root
    /uuids
        get ?type=(short|v1|v4),length=(int),count=(int)
            type default v4
            length default 8
            count default 1
        
        /convert
            post 
                [
                    {
                        uuid: str,
                            default null so random
                        length: int
                            default 8
                        alphaNumeric: bool
                            default false
                    },
                    ...
                ]
    /words
        get ?count=(int)
    /jumble
        post
            [
                any
            ]
    /passwords
        get ?count=(int),length=(int),easyRead=(bool),type=(1|2|3)
        post
            {
                count: int,
                length: int,
                easyRead: bool,
                type: 0,1,2,3,
                chars: list<char>
            }
    /passphrases
        get ?count=(int),length=(int),separator=(char)
        post
            {
                count: int,
                length: int,
                separators: list<char>
                words: list<stings>
            }
    /colors
        get ?count=(int),type=(hex|rgb|cmyk)
    /numbers
        get ?count=(int),min=(int),max=(int)
    /cards
        get ?count=(int),jokers=(bool),deck=(standard|pinochle)
        
        /decks
            get ?count=(int),jokers=(bool),deck=(standard|pinochle)
    /seeds
        get ?count=(int),bytes=(int)
```