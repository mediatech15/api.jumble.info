const rateLimit = require("express-rate-limit");
const RedisStore = require("rate-limit-redis");
const { createClient } = require("redis");

let u = `redis://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`
const client = createClient({
    url: u
});
client.connect();

const limiter = rateLimit({
    windowMs: 1000 * 60,
    max: 60,
    standardHeaders: true,
    legacyHeaders: false,

    store: new RedisStore({
        sendCommand: (...args) => client.sendCommand(args),
    }),
});

module.exports = limiter;
