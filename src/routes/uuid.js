const express = require('express')
const { getShort, getV1, getV4, convertUUID } = require('../logic/uuid')

function getUuidRoutes() {
    const router = express.Router()
    router.get('/', uuid)
    router.post('/convert', uuidConvert)
    return router
}

async function uuid(req, res) {
    try {
        let type = req.query.type || 'v4'
        let length = req.query.length || 8
        let count = req.query.count || 1
        let alpha = req.query.alpha || false
        if (type == 'v4') {
            res.status(200).send(getV4(count))
        } else if (type == 'v1') {
            res.status(200).send(getV1(count))
        } else if (type == 'short') {
            res.status(200).send(getShort(count, length, alpha))
        } else {
            res.status(400)
        }
    } catch {
        res.status(400)
    }
}

async function uuidConvert(req, res) {
    try {
        if (req.body) {
            res.status(200).send(convertUUID(req.body))
        } else {
            res.status(400)
        }
    } catch {
        res.status(400)
    }
}

module.exports = { getUuidRoutes }