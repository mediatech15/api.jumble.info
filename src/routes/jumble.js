const express = require('express')

function getJumbleRoutes() {
    const router = express.Router()
    router.get('/', jumble)
    return router
}

async function jumble(req, res) {
    res.send({ response: 'sdfafa', request: req.originalUrl, query: req.query })
}

module.exports = { getJumbleRoutes }