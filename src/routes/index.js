const express = require('express')
const { getTestRoutes } = require('./test')
const { getCardRoutes } = require('./card')
const { getColorRoutes } = require('./color')
const { getJumbleRoutes } = require('./jumble')
const { getNumberRoutes } = require('./number')
const { getPassphraseRoutes } = require('./passphrase')
const { getPasswordRoutes } = require('./password')
const { getUuidRoutes } = require('./uuid')
const { getWordRoutes } = require('./word')
const { getSeedRoutes } = require('./seed')

function getRoutes() {
    const router = express.Router()

    router.use('/test', getTestRoutes())
    router.use('/cards', getCardRoutes())
    router.use('/colors', getColorRoutes())
    router.use('/jumble', getJumbleRoutes())
    router.use('/numbers', getNumberRoutes())
    router.use('/passphrases', getPassphraseRoutes())
    router.use('/passwords', getPasswordRoutes())
    router.use('/uuids', getUuidRoutes())
    router.use('/words', getWordRoutes())
    router.use('/seeds', getSeedRoutes())

    return router
}

module.exports = getRoutes