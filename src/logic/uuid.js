const { v1, v4 } = require('uuid')
const { nanoid, customRandom, urlAlphabet, customAlphabet } = require('nanoid')
const seedrandom = require('seedrandom')
let alphaNumeric = 'useandom26T198340PX75pxJACKVERYMINDBUSHWOLFGQZbfghjklqvwyzrict'

function getV4(count) {
    count = ~~Math.max(0, parseInt(count)) || 1
    let response = []
    do {
        response.push(v4())
    } while (response.length < count)
    return response
}

function getV1(count) {
    count = ~~Math.max(0, parseInt(count)) || 1
    let response = []
    do {
        response.push(v1())
    } while (response.length < count)
    return response
}

function getShort(count, length, alpha) {
    count = ~~Math.max(0, parseInt(count)) || 1
    length = ~~Math.max(0, parseInt(length)) || 8
    alpha = (alpha === 'true')
    let response = []
    do {
        if (alpha) {
            let n = customAlphabet(alphaNumeric, length)
            response.push(n())
        } else {
            response.push(nanoid(length))
        }
    } while (response.length < count)
    return response
}

function convertUUID(ids) {
    let response = []
    for (let i of ids) {
        let u = i['uuid'] || null
        let l = ~~Math.max(0, parseInt(i['length'])) || 8
        let a = i['alphaNumeric'] || false
        const rng = seedrandom(u)
        if (a) {
            const nanoid = customRandom(alphaNumeric, l, size => {
                return (new Uint8Array(size)).map(() => 256 * rng())
            })
            response.push(nanoid())
        } else {
            const nanoid = customRandom(urlAlphabet, l, size => {
                return (new Uint8Array(size)).map(() => 256 * rng())
            })
            response.push(nanoid())
        }
    }
    return response
}

module.exports = { getShort, getV1, getV4, convertUUID }