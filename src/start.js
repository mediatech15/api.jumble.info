const express = require('express')
const rateLimiterRedisMiddleware = require('./middleware/rateLimit');
require('express-async-errors')
const getRoutes = require('./routes')
const morgan = require('morgan')
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet')

function startServer({ port = process.env.PORT } = {}) {
    const app = express()
    app.use(morgan('dev'))
    app.use(cors())
    app.use(helmet())
    app.use(errorMiddleware)
    app.use(rateLimiterRedisMiddleware)
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(bodyParser.json())
    app.use('/', getRoutes())
    return new Promise(resolve => {
        const server = app.listen(port, () => {
            console.log(`Listening on port ${server.address().port}`)

            const originalClose = server.close.bind(server)
            server.close = () => {
                return new Promise(resolveClose => {
                    originalClose(resolveClose)
                })
            }
            setupCloseOnExit(server)
            resolve(server)
        })
    })
}

function errorMiddleware(error, req, res, next) {
    if (res.headersSent) {
        next(error)
    } else {
        console.error(error)
        res.status(500)
        res.json({
            message: error.message,
            originalRequest: req,
            ...(process.env.NODE_ENV === 'production' ? null : { stack: error.stack }),
        })
    }
}

function setupCloseOnExit(server) {
    async function exitHandler(options = {}) {
        await server
            .close()
            .then(() => {
                console.log('Server successfully closed')
            })
            .catch(e => {
                console.warn('Something went wrong closing the server', e.stack)
            })

        if (options.exit) process.exit()
    }
    process.on('exit', exitHandler)
    process.on('SIGINT', exitHandler.bind(null, { exit: true }))
    process.on('SIGUSR1', exitHandler.bind(null, { exit: true }))
    process.on('SIGUSR2', exitHandler.bind(null, { exit: true }))
    process.on('uncaughtException', exitHandler.bind(null, { exit: true }))
}

module.exports = { startServer }